1. Visit https://gitlab.com/saidatech_learn/test-scripting/-/blob/main/run.py on your browser
2. click download on the right side 
3. 

# for mac users run each of the below
open terminal and cd into your Downloads folder
python3 -m venv venv
source venv/bin/activate
pip install boto3
python3 run.py


# for windows users run each of the below
open powershell and cd into your Downloads folder
py -m venv venv
./venv/Scripts/activate
pip install boto3
py run.py
